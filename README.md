**EDIT**: explanation and solution added at the top, read further down for the
original problem.

# Explanation of promises behaviour
this is a known NodeJS bug with promises that will never resolve
(possibly related to [pending tasks on the event
loop](https://github.com/nodejs/node/issues/22088#issuecomment-410165867)). A
nice MVP is mentioned in [this
comment](https://github.com/nodejs/node/issues/22088#issuecomment-409966419),
which I've added to this repo as `mvp.mjs`. Seems other people hit this is
[stream related
code](https://github.com/nodejs/node/issues/22088#issuecomment-465380046) too.

Good news, when you use modules and top level `await` (not sure if both are
required), you'll get a non-zero return code in this case. Here's how you can
test this for yourself:

New way, gives non-zero return code:
```bash
 # top level await in module test
cat <<"HEREDOC" | docker run --rm -i --entrypoint sh node:17.6-alpine && echo "zero return code"
f=/tmp/blah.mjs
echo "async function blah() {" >> $f
echo "  await new Promise(r => console.log(42))" >> $f
echo "  console.log('hello')" >> $f
echo "}" >> $f
echo "await blah()" >> $f
cat $f
node $f
HEREDOC
```

Old way, silently fails:
```bash
 # non-module syntax test
cat <<"HEREDOC" | docker run --rm -i --entrypoint sh node:17.6-alpine && echo "zero return code"
f=/tmp/blah.js
echo ";(async function () {" >> $f
echo "  await new Promise(r => console.log(42))" >> $f
echo "  console.log('hello')" >> $f
echo "})()" >> $f
cat $f
node $f
HEREDOC
```

# How to fix this (correct stream usage)
The problem was that the correct event name for the WriteableStream is `finish`
*not* `end`. This diff shows what's required to make everything work as expected:
```diff
       console.log('read stream "end" event triggered')
     })
     writeStream.on('error', reject)
-    writeStream.on('end', () => {
+    writeStream.on('finish', () => {
       // FIXME why doesn't this trigger?
-      console.log('write stream "end" event triggered')
+      console.log('write stream "finish" event triggered')
       console.log('Finished writing PDF to local disk')
```

---

# Original rant

This is a minimal reproduction of a bug when the NodeJS VM exits without
finishing a promise. Finishing meaning one of resolve or reject. I thought at
least one was guaranteed to happen, but I guess not.

Versions of node I've testing this behaviour on:
- 14.17
- 17.6

# Steps
- clone repo
- launch docker with a known Node version. Or you could use `nvm` if you have it
    ```bash
    docker run --rm -it -v $PWD:/app -w /app -u $(id -u):$(id -g) node:14.17 bash
    ```
- install deps
    ```bash
    yarn install
    ```
- run the test
    ```bash
    node .
    ```

# Expected
The logs from the `end` event on the write stream and the `.then()` on the
promise are printed (the last three lines):
```
creating PDF doc
piping to write stream
flushing read stream, waiting for "end" event on write stream
end of Promise, waiting for resolve() or reject()
read stream "end" event triggered
write stream "end" event triggered
Finished writing PDF to local disk
done, PDF written to ${filepath}
```

Interestingly, the PDF *is* still written, and it's complete.

# Actual
The NodeJS vm exits with a `0` return code but the promise was never finished.
We didn't have our `.then()` or `.catch()` called. The last 3 log lines (above)
are not printed either.

# Possible fix
Using `await doc.end()` instead of hooking the `end` event inside `new Promise`
seems to fix things. I'm not sure if waiting for the end of the read stream
will also guarantee that the write stream has finished flushing.

To test this
```bash
node . fix
```

# More
The `wait.js` script checks the assumption that the NodeJS VM *will* wait for
promises to finish before exiting. It will.
