// tests the assumption that the VM will wait for promises to finish

async function doit() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Math.random() > 0.5) {
        return resolve()
      }
      return reject()
    }, 1500)
  })
}

doit()
  .then(() => console.log('success'))
  .catch(() => {
    console.error('fail')
    process.exit(1)
  })
