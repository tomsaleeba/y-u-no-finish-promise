import fs from 'fs'
import pdf from 'pdfjs'

const filepath = '/tmp/out.pdf'
const isUseFix = !!process.argv[2]

async function writeTest() {
  fs.rmSync(filepath, {force: true})
  console.log('creating PDF doc')
  const doc = new pdf.Document()
  doc.text(new Date().toString())
  for (let i = 0; i < 1000; i++) {
    doc.text(`${i} Lorem Ipsum`)
  }
  const writeStream = fs.createWriteStream(filepath)
  console.log('piping to write stream')
  doc.pipe(writeStream)
  if (isUseFix) {
    console.log('Using "possible fix" strategy that appears to work')
    // works, but does this guarantee the *write* stream is done and flushed? \/
    await doc.end()
    return
    // works, but does this guarantee the *write* stream is done and flushed? /\
  }
  // does NOT work, VM just exits without finishing the promise \/
  return new Promise((resolve, reject) => {
    doc.on('error', reject)
    doc.on('end', () => {
      console.log('read stream "end" event triggered')
    })
    writeStream.on('error', reject)
    writeStream.on('end', () => { // note: the fix is to use the `finish` event
      // FIXME why doesn't this trigger?
      console.log('write stream "end" event triggered')
      console.log('Finished writing PDF to local disk')
      // FIXME how does this promise finish without resolve or reject being called?
      return resolve()
    })
    console.log('flushing read stream, waiting for "end" event on write stream')
    doc.end()
      // this doesn't help either
      // .then(() => {
      //   console.log('ending write stream')
      //   writeStream.end()
      //   return new Promise(r => setTimeout(r, 1000))
      // })
    console.log('end of Promise, waiting for resolve() or reject()')
  })
  // does NOT work, VM just exits without finishing the promise /\
}

writeTest()
  .then(() => {
    // FIXME if the promise doesn't reject, why isn't this called?
    console.log(`done, PDF written to ${filepath}`)
  })
  .catch(e => {
    console.error('Fail town', e)
    process.exit(1)
  })
