;(async function () {
  // note: r() is never called
  await new Promise(r => console.log(42))
  // await new Promise(r => setTimeout(r, 1000)) // using this instead works as expected
  console.log("you'll never see this, and there won't be an error :'( ")
})()
